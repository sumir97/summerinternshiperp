package actions.feePayment;


import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FeeBreakup
 */

@WebServlet(
		name="Fee break up Servlet",
		urlPatterns={"/FeeBreakup"}
	)
public class FeeBreakup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FeeBreakup() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(500);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		int sem = Integer.parseInt(request.getParameter("semester"));
		int year = Integer.parseInt(request.getParameter("year"));
		String breakupGen=request.getParameter("fee_breakup_general");
		String breakupSc=request.getParameter("fee_breakup_sc");
		breakupGen = URLDecoder.decode(breakupGen, "UTF-8");
		breakupSc  = URLDecoder.decode(breakupSc, "UTF-8");
		
		System.out.println(sem+"\n"+year+"\n"+breakupGen+"\n"+breakupSc);
		
  postgreSQLDatabase.feePayment.Query.addFeeBreakup(sem,"GENERAL",breakupGen,year);
  postgreSQLDatabase.feePayment.Query.addFeeBreakup(sem,"SC",breakupSc,year);
  postgreSQLDatabase.feePayment.Query.addFeeBreakup(sem,"ST",breakupSc,year);
  postgreSQLDatabase.feePayment.Query.addFeeBreakup(sem,"OBC",breakupGen,year);
    
	
	}

}
