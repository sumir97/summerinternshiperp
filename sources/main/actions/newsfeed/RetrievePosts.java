package actions.newsfeed;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import actions.authentication.Session;
import postgreSQLDatabase.newsfeed.NewsfeedComments;
import postgreSQLDatabase.newsfeed.NewsfeedPost;

/**
 * Servlet implementation class RetrievePosts
 */
@WebServlet("/RetrievePosts")
public class RetrievePosts extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RetrievePosts() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		PrintWriter writer=response.getWriter();
		
		if(request.getParameter("action").equals("retrievePosts")){
		long erp_id=Long.parseLong(request.getSession().getAttribute("erpId").toString());
		 ArrayList<NewsfeedPost> all_posts = postgreSQLDatabase.newsfeed.Query.getNewsfeedAllPosts(erp_id);
		 
		 JSONArray newsfeed=new JSONArray();
		 Iterator<NewsfeedPost> iterator = all_posts.iterator();
		 while(iterator.hasNext()){
			 NewsfeedPost current=iterator.next();
			 
			 JSONArray j_array=new JSONArray();
		     SimpleDateFormat df=new SimpleDateFormat("dd MMM yy hh:mm");
		     
			 ArrayList<NewsfeedComments> all_comments = postgreSQLDatabase.newsfeed.Query.getNewsfeedPostComments(current.getPost_id());
			Iterator<NewsfeedComments> comment_iterator = all_comments.iterator();
			 while(comment_iterator.hasNext()){
				 JSONObject j_obj=new JSONObject();
				 NewsfeedComments current_comment = comment_iterator.next();
				 String comment_author=postgreSQLDatabase.authentication.Query.getUserName(current_comment.getComment_author());
				 j_obj.put("comment_author", comment_author);
				 j_obj.put("comment_author_username",postgreSQLDatabase.authentication.Query.getUserUsername(current_comment.getComment_author()));
				 j_obj.put("comment_id", current_comment.getComment_id());
				 j_obj.put("comments_likes_count", current_comment.getComments_count());
				 ArrayList<Long> comment_likes=current_comment.getComment_likes();
				 j_obj.put("comment_likes", comment_likes);
				 j_obj.put("comment_text", current_comment.getComment_text());
				 
				 if(comment_likes.contains(Long.parseLong(request.getSession().getAttribute("erpId").toString()))){
						j_obj.put("selfCommentLike", true);
					}
					else{
						j_obj.put("selfCommentLike", false);
					}
				 
				 
				 j_obj.put("comment_time", df.format(current_comment.getComment_time()));
				 j_array.put(j_obj);
			 }
			 JSONObject current_post=new JSONObject();
			current_post.put("post_id", current.getPost_id());
			current_post.put("post_text", current.getPost_text());
			String post_author=postgreSQLDatabase.authentication.Query.getUserName(current.getPost_author());
			String post_author_username=postgreSQLDatabase.authentication.Query.getUserUsername(current.getPost_author());
			current_post.put("post_author",post_author);
			current_post.put("post_author_username",post_author_username);
			current_post.put("post_privacy", current.getPost_privacy());
			current_post.put("post_time", df.format(current.getPost_time()));
			current_post.put("post_comments", j_array.toString());
			ArrayList<Long> likes=current.getPost_likes();
			current_post.put("post_likes", current.getPost_likes());
			if(likes.contains(Long.parseLong(request.getSession().getAttribute("erpId").toString()))){
				current_post.put("selfLike", true);
			}
			else{
				current_post.put("selfLike", false);
			}
			current_post.put("likes_count", current.getLikes_count());
			current_post.put("comments_count", current.getComments_count());
			newsfeed.put(current_post);
		 }
		 
		writer.write(newsfeed.toString());
	}
		
		
		if(request.getParameter("action").equals("addPost")){
			//long author=Long.parseLong(request.getParameter("author"));
			long author=Long.parseLong(request.getSession().getAttribute("erpId").toString());
			String text=request.getParameter("text").toString();
			int privacy=Integer.parseInt(request.getParameter("privacy"));
			long post_id=postgreSQLDatabase.newsfeed.Query.addNewsfeedPost(text,author,privacy);
			if(privacy==0){
				postgreSQLDatabase.newsfeed.Query.sendNewsfeedPostByGroup(post_id,request.getSession().getAttribute("usertype").toString());
			}
		}
		
		
		
		if(request.getParameter("action").equals("addComment")){
			long author=Long.parseLong(request.getSession().getAttribute("erpId").toString());
			String text=request.getParameter("text").toString();
			long post_id=Integer.parseInt(request.getParameter("post_id"));
			postgreSQLDatabase.newsfeed.Query.addNewsfeedComment(text, author, post_id);
		}
		
		if(request.getParameter("action").equals("setLike")){
			long liker=Long.parseLong(request.getSession().getAttribute("erpId").toString());
			long post_id=Integer.parseInt(request.getParameter("post_id"));
			postgreSQLDatabase.newsfeed.Query.setLike(post_id,liker);
		}
		
		if(request.getParameter("action").equals("like_comment")){
			long liker=Long.parseLong(request.getSession().getAttribute("erpId").toString());
			long post_id=Integer.parseInt(request.getParameter("comment_id"));
			postgreSQLDatabase.newsfeed.Query.setCommentLike(post_id,liker);
		}
		
		if(request.getParameter("action").equals("unlike_comment")){
			long liker=Long.parseLong(request.getSession().getAttribute("erpId").toString());
			long comment_id=Integer.parseInt(request.getParameter("comment_id"));
			postgreSQLDatabase.newsfeed.Query.setCommentUnlike(comment_id,liker);
		}
		
	}
	

}
