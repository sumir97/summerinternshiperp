/**
 * 
 */
package postgreSQLDatabase.gradingModule;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * @author Dilip
 *
 */
public class Query {


	public static ArrayList<Subject> getSubjects(int semester) throws SQLException{
		
		ArrayList<Subject> list = new ArrayList<Subject>();
		
		
		
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"getSubjects\"(?);");
			
			proc.setInt(1,semester);
			ResultSet rs = proc.executeQuery();


			while(rs.next())
			{
				
				Subject subject = new Subject();
				
				subject.setCourse_code(rs.getString("course_code"));
				subject.setSemester(rs.getInt("semester"));
				subject.setCourse_name(rs.getString("course_name"));
				subject.setCredit(rs.getInt("credit"));
				
				list.add(subject);
			}
		
			rs.close();
			proc.close();
		
		
		return list;
	}
public static ArrayList<Grade> getGradeList(String course_code,Integer year) throws SQLException{
		
		ArrayList<Grade> list = new ArrayList<Grade>();
		
		
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getCourseGradeList\"(?,?);");
			
			proc.setString(1,course_code);
			proc.setInt(2,year);
			ResultSet rs = proc.executeQuery();

			while(rs.next())
			{
				
				
				Grade current= new Grade();
				
				current.setCourse_code(rs.getString("course_code"));
				current.setStudent_id(rs.getString("student_id"));
				current.setStudent_name(rs.getString("student_name"));
				current.setStudent_grade(rs.getString("grade"));
				
				
				list.add(current);
			}
		}  catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}

public static void generateCsvFile(String sFileName , String sID , String sName , String grade){
	try
	{
	    FileWriter writer = new FileWriter(sFileName , true);
	    
	    writer.append(sID);
	    writer.append(',');
	    writer.append(sName);
	    writer.append(',');
	    writer.append(grade);
	    writer.append('\n');
			
	    writer.flush();
	    writer.close();
	}
	catch(IOException e)
	{
	     e.printStackTrace();
	} 
 }
}
