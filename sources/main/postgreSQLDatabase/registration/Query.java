/**
 * 
 */
package postgreSQLDatabase.registration;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.postgresql.util.PGobject;

import exceptions.IncorrectFormatException;
import settings.database.PostgreSQLConnection;
import users.Student;
import utilities.UsernamePermutation;

/**
 * @author Anshula
 *
 */
public class Query {

	public static void updateVerificationStatus(int status, long reg_id) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"updateVerificationStatus\"(?,?);");
			proc.setInt(1, status);
			proc.setLong(2, reg_id);
			System.out.println(proc);
			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void setProfilePicture(long erp_id, long file_id) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"setProfilePicture\"(?,?);");
			proc.setLong(1, erp_id);
			proc.setLong(2, file_id);
			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static ArrayList<Student> getStudentRegistrationList() throws SQLException, IncorrectFormatException {
		ArrayList<Student> students = null;
		Student current;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getStudentRegistrationList\"() ;");
			students = new ArrayList<Student>();
			ResultSet rs = proc.executeQuery();
			while (rs.next()) {
				current = new Student(rs);
				students.add(current);

			}

			rs.close();
			proc.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return students;
	}

	/**
	 * @param username
	 * @param name
	 * @param user_type
	 * @return erp id
	 */
	public static String registerUser(String username, String name, String user_type) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"addUser\"(?,?,?);");
			proc.setString(1, username);
			proc.setString(2, name);
			proc.setString(3, user_type);
			ResultSet rs = proc.executeQuery();
			rs.next();
			String erp_id = String.valueOf(rs.getObject(1));
			return erp_id;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";

	}

	public static ArrayList<Student> getCsabStudentList() throws SQLException, IncorrectFormatException {
		ArrayList<Student> students = null;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"displayCsabList\"();");

			students = new ArrayList<Student>();
			ResultSet rs = proc.executeQuery();

			while (rs.next()) {
				Student current = new Student(rs, true);

				students.add(current);
			}
			Iterator<Student> iterator = students.iterator();
			while (iterator.hasNext()) {
				Student current = iterator.next();
			}

			rs.close();
			proc.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return students;
	}

	public static JSONObject getRegistrationStudentDataJSON(long reg_id) throws SQLException, IncorrectFormatException {
		Student student = getRegistrationStudentData(reg_id);

		JSONObject current_object = student.getJSON();
		return current_object;
	}

	public static JSONObject getCsabStudentProfileJSON(long csab_id) throws SQLException, IncorrectFormatException {
		JSONObject current = null;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"displayCsabProfile\"(?);");
			proc.setLong(1, csab_id);
			ResultSet rs = proc.executeQuery();
			rs.next();
			Student student = new Student(rs);
			current = student.getJSON();
			rs.close();
			proc.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return current;
	}

	public static Student getCsabStudentProfile(long csab_id) throws SQLException, IncorrectFormatException {
		Student student = null;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"displayCsabProfile\"(?);");
			proc.setLong(1, csab_id);
			ResultSet rs = proc.executeQuery();
			rs.next();
			student = new Student(rs);

			rs.close();
			proc.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return student;
	}

	public static Student getRegistrationStudentData(Long reg_id) throws SQLException, IncorrectFormatException {

		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getRegistrationStudentData\"(?);");
			proc.setLong(1, reg_id);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			rs.next();
			Student current = new Student(rs);

			rs.close();
			proc.close();
			return current;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public static Student getStudentProfile(Long erp_id) throws SQLException, IncorrectFormatException {
		Student current = null;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getStudentProfile\"(?);");
			proc.setLong(1, erp_id);

			ResultSet rs = proc.executeQuery();
			rs.next();

			current = new Student(rs);

			rs.close();
			proc.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return current;
	}

	public static JSONArray retrieveRegistrationData() throws SQLException {

		PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
				.prepareStatement("SELECT public.\"displayRegistrationList\"();");
		ResultSet rs = proc.executeQuery();
		// System.out.println(proc);
		rs.next();

		JSONArray jArray = new JSONArray(rs.getString(1));

		return jArray;
	}

	public static void updateVerified(Long reg_id) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"updateVerified\"(?);");
			proc.setLong(1, reg_id);

			proc.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static JSONObject getRegistrationStudentDataUpdateJSON(Long reg_id)
			throws SQLException, IncorrectFormatException {
		Student current = getStudentRegistrationDataUpdate(reg_id);
		// System.out.println(current.getJSON());

		return current.getJSON();

	}

	public static Student getStudentRegistrationDataUpdate(Long reg_id) throws SQLException, IncorrectFormatException {
		Student current = null;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from  public.\"getStudentRegistrationDataUpdate\"(?);");
			proc.setLong(1, reg_id);

			ResultSet rs = proc.executeQuery();
			rs.next();

			current = new Student(rs, true);

			rs.close();
			proc.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return current;
	}

	public static void addUpdateStudentRegistrationDetails(Student student) throws SQLException {
		PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement(
				"SELECT public.\"addUpdateRegistrationStudentDetails\"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
		// text, text, text, text, text, integer, text, text, text, text, text,
		// text, text, text, boolean, json, text, text

		proc.setString(1, student.getFirst_name());
		proc.setString(2, student.getMiddle_name());
		proc.setString(3, student.getLast_name());
		proc.setString(4, student.getMobile());
		proc.setString(5, student.getEmail());
		proc.setLong(6, student.getRegistration_id());
		proc.setString(7, student.getGuardian_name());
		proc.setString(8, student.getGuardian_contact());
		proc.setString(9, student.getGuardian_email());
		proc.setString(10, student.getGuardian_address());
		proc.setString(11, student.getFather_name());
		proc.setString(12, student.getMother_name());
		proc.setString(13, student.getFather_contact());
		proc.setString(14, student.getMother_contact());

		proc.setBoolean(15, student.isHosteller());
		JSONObject address_obj = new JSONObject();
		address_obj.put("room", student.getRoom());
		address_obj.put("hostel", student.getHostel());
		PGobject jsonObject = new PGobject();
		jsonObject.setType("json");
		jsonObject.setValue(address_obj.toString());

		proc.setObject(16, jsonObject);
		proc.setString(17, student.getPermanent_address());
		proc.setString(18, student.getLocal_address());
		/*
		 * proc.setBoolean(28,student.isApplied());
		 * proc.setDate(29,student.getEntry_time());
		 */
		System.out.println(proc);
		proc.executeQuery();
	}

	public static void applyUpdate(long reg_id) throws SQLException {
		PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
				.prepareStatement("SELECT public.\"applyUpdate\"(?);");
		proc.setLong(1, reg_id);
		proc.executeQuery();
	}

	public static int retrieveRegistrationStatus(Long reg_id) {

		try {
			int status = getRegistrationStudentData(reg_id).getVerification_status();
			return status;
			// proc =
			// settings.database.PostgreSQLConnection.settings.database.PostgreSQLConnection.getConnection()
			// .prepareStatement("SELECT public.\"existsRegId\"(?);");
			// proc.setLong(1, reg_id);
			// ResultSet rs = proc.executeQuery();
			// rs.next();
			//
			// System.out.println("ID exists " + rs.getBoolean(1));
			// if (rs.getBoolean(1)) {
			// proc =
			// PostgreSQLConnection.settings.database.PostgreSQLConnection.getConnection()
			// .prepareStatement("SELECT
			// public.\"retrieveRegistrationStatus\"(?);");
			// proc.setLong(1, reg_id);
			// rs = proc.executeQuery();
			// rs.next();
			// boolean verified = rs.getBoolean(1);
			// System.out.println("verified " + verified);
			// if (verified) {
			// return 1;
			// } else {
			// return 0;
			// }
			//
			// } else {
			// return -1;
			// }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return -1;

	}

	public static Long reportStudent(Long csab_id) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"report_student\"(?);");

			proc.setLong(1, csab_id);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			rs.next();
			Long reg_id = rs.getLong(1);
			return reg_id;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void retrieveStudentList() {
		PreparedStatement proc;
		try {
			proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT \"retrieveStudentList\"();");

			ResultSet rs = proc.executeQuery();
			rs.next();
			JSONArray student_list = new JSONArray(rs.getString(1));
			int i = 0;
			JSONObject student;
			for (i = 0; i < student_list.length(); i++) {
				student = student_list.getJSONObject(i);

				Student current = new Student();
				current.setStudent_id(student.get("student_id").toString());
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static ArrayList<String> getUsernameGenerationData(long reg_id) {
		UsernameGeneration ug = null;
		ArrayList<String> usernames = new ArrayList<String>();
		ArrayList<String> user = new ArrayList<String>();
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getUsernameGenerationData\"(?);");
			proc.setLong(1, reg_id);
			ResultSet rs = proc.executeQuery();
			rs.next();
			ug = new UsernameGeneration();

			ug.setFirst(rs.getString("first_name"));
			ug.setMiddle(rs.getString("middle_name"));
			ug.setLast(rs.getString("last_name"));
			if(rs.getString("last_name").equals(""))ug.setLast("cse");
			ug.setProgram_allocated(rs.getString("program_allocated"));
			ug.setReg_year(rs.getInt("reg_year"));
			ug.setBirth_year(rs.getInt("birth_year"));
			System.out.println(ug.getFirst() + "  " + ug.getLast() + "  " + String.valueOf(ug.getBirth_year()));
			int count = 0;
			usernames = UsernamePermutation.generatePermutations(ug.getFirst(), ug.getLast(),
					String.valueOf(ug.getBirth_year()));
			Iterator<String> iterator = usernames.iterator(); /// checkifusernameexists(username_generation
																/// table)
			while (iterator.hasNext() && count < 5) { /// select max 5
														/// usernames(serially)
				String username = iterator.next();
				if (checkIfUsernameExists(username)) {
					// usernames.remove(username);
					continue;
				} else {
					user.add(username);
					count++;
				}
			}

			/*
			 * Iterator<String> iter=usernames.iterator(); while(iter.hasNext()
			 * && count<5){ user.add(iter.next()); count++; }
			 */
			insertWithValueFalse(user); //// insert these 5 usernames in
										//// username_genaeration with value
										//// false

			proc.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}

	public static String getStudentId(long reg_id) {
		PreparedStatement proc;
		try {
			proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT \"getStudentId\"(?);");

			proc.setLong(1, reg_id);
			ResultSet rs = proc.executeQuery();
			rs.next();
			return rs.getString(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public static void insertWithValueFalse(ArrayList<String> user) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"addUsernameGeneration\"(?);");
			proc.setArray(1, PostgreSQLConnection.getConnection().createArrayOf("text", user.toArray()));
			ResultSet rs = proc.executeQuery();
			rs.next();
			proc.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean checkIfUsernameExists(String username) {
		boolean status = false;
		;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"getUsernameStatus\"(?);");
			proc.setString(1, username);
			ResultSet rs = proc.executeQuery();
			rs.next();

			status = rs.getBoolean(1);
			System.out.println(status);
			proc.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return status;
	}

	public static void main(String[] args) throws SQLException, IncorrectFormatException {
		// retrieveStudentList();
		//reportStudent(52l);
		//getStudentsListByTransacation();
			//getRegistrationStudentDataUpdateJSON(65L);
		//System.out.println(getRegistrationId(1000000106));
		
	}

	public static ArrayList<String> getUserList(String type) throws SQLException {
		PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
				.prepareStatement("SELECT * from \"getUsersByType\"(?)");
		proc.setString(1, type);
		ResultSet rs = proc.executeQuery();
		ArrayList<String> usernames = new ArrayList<String>();
		while (rs.next()) {
			usernames.add(rs.getString("username"));
		}
		return usernames;

	}

	/**
	 * @param username
	 */
	public static void markUsernameTrue(String username) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"useUserNameGeneration\"(?);");
			proc.setString(1, username);
			ResultSet rs = proc.executeQuery();
			rs.next();
			proc.close();

			PreparedStatement proc1 = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"deleteFalseUsername\"();");
			ResultSet rs1 = proc1.executeQuery();
			rs1.next();

			proc.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void deleteCSABStudent(long csab_id) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"deleteCSABStudent\"(?);");
			proc.setLong(1, csab_id);

			proc.executeQuery();
			System.out.println("rows affected= " + proc.getUpdateCount());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void promoteStudent(long reg_id) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"promoteStudent\"(?);");
			proc.setLong(1, reg_id);
			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void updateCSABStudentData(Student student) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement(
					"SELECT public.\"updateCsabStudentData\"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
			proc.setLong(1, student.getCsab_id());
			proc.setString(2, student.getName());
			proc.setString(3, student.getFirst_name());
			proc.setString(4, student.getMiddle_name());
			proc.setString(5, student.getLast_name());
			proc.setString(6, student.getCategory());
			proc.setLong(7, student.getJee_main_rollno());
			proc.setLong(8, student.getJee_adv_rollno());
			proc.setString(9, student.getState_eligibility());
			proc.setString(10, student.getMobile());
			proc.setString(11, student.getEmail());
			// System.out.println("date is
			// :"+utilities.StringFormatter.convert(date_of_birth));
			proc.setDate(12, utilities.StringFormatter.convert(student.getDate_of_birth()));
			proc.setString(13, student.getProgram_allocated());
			proc.setString(14, student.getAllocated_category());
			proc.setString(15, student.getAllocated_category());
			proc.setString(16, student.getStatus());
			proc.setInt(17, student.getChoice_no());
			proc.setBoolean(18, student.isPwd());
			proc.setString(19, student.getGender());
			proc.setString(20, student.getQuota());
			proc.setInt(21, student.getRound());
			proc.setString(22, student.getWillingness());
			proc.setString(23, student.getPermanent_address());
			proc.setString(24, student.getRc_name());
			proc.setString(25, student.getNationality());
			System.out.println(proc);
			proc.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @param csab_id
	 * @param name
	 * @param first_name
	 * @param middle_name
	 * @param last_name
	 * @param category
	 * @param jee_main_rollno
	 * @param jee_advance_rollno
	 * @param state
	 * @param phone_number
	 * @param email
	 * @param date_of_birth
	 * @param program_allocated
	 * @param allocated_category
	 * @param allocated_rank
	 * @param status
	 * @param choice_number
	 * @param pwd
	 * @param gender
	 * @param quota
	 * @param round
	 * @param willingness
	 * @param address
	 * @param rc_name
	 * @param nationality
	 * @param entry_date
	 * @param reported
	 */
	public static void addCSABStudentData(Student current) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT \"addCSABData\"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
			proc.setString(1, current.getName()); // 1 is the first ? (1 based
													// counting)
			proc.setString(2, current.getFirst_name());
			proc.setString(3, current.getMiddle_name());
			proc.setString(4, current.getLast_name());
			proc.setString(5, current.getCategory());
			proc.setInt(6, current.getJee_main_rollno());
			proc.setInt(7, current.getJee_adv_rollno());
			proc.setString(8, current.getState_eligibility());
			proc.setString(9, current.getMobile());
			proc.setString(10, current.getEmail());
			proc.setDate(11, utilities.StringFormatter.convert(new java.util.Date()));
			proc.setString(12, current.getProgram_allocated());
			proc.setString(13, current.getAllocated_category());
			proc.setString(14, current.getAllocated_rank());
			proc.setString(15, current.getStatus());
			proc.setInt(16, current.getChoice_no());
			proc.setBoolean(17, current.isPwd());
			proc.setString(18, current.getGender());
			proc.setString(19, current.getQuota());
			proc.setInt(20, current.getRound());
			proc.setString(21, current.getWillingness());
			proc.setString(22, current.getPermanent_address());
			proc.setString(23, current.getRc_name());
			proc.setString(24, current.getNationality());
			System.out.println(proc);
			proc.executeQuery();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static ArrayList<Student> getStudent() throws SQLException, IncorrectFormatException {
		ArrayList<Student> students = null;
		Student current;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT * from public.\"getStudentRegistrationList\"();");
			students = new ArrayList<Student>();
			ResultSet rs = proc.executeQuery();
			while (rs.next()) {
				current = new Student(rs);
				students.add(current);

			}

			rs.close();
			proc.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return students;
	}

	public static void updateRegisteredStudentData(Student student) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement(
					"SELECT public.\"updateRegisteredStudentData\"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
			proc.setString(1, student.getName());
			proc.setString(2, student.getFirst_name());
			proc.setString(3, student.getMiddle_name());
			proc.setString(4, student.getLast_name());
			proc.setString(5, student.getCategory());
			proc.setString(6, student.getState_eligibility());
			proc.setString(7, student.getMobile());
			proc.setString(8, student.getEmail());
			// System.out.println("date is
			// :"+utilities.StringFormatter.convert(date_of_birth));
			proc.setDate(9, utilities.StringFormatter.convert(student.getDate_of_birth()));
			proc.setString(10, student.getProgram_allocated());
			proc.setString(11, student.getStatus());
			proc.setBoolean(12, student.isPwd());
			proc.setString(13, student.getGender());
			proc.setString(14, student.getNationality());
			proc.setLong(15, student.getRegistration_id());
			proc.setString(16, student.getGuardian_name());
			proc.setString(17, student.getGuardian_contact());
			proc.setString(18, student.getGuardian_email());
			proc.setString(19, student.getGuardian_address());
			proc.setString(20, student.getFather_name());
			proc.setString(21, student.getFather_contact());
			proc.setString(22, student.getMother_contact());
			proc.setString(23, student.getMother_name());
			proc.setBoolean(24, student.isHosteller());
			PGobject obj = new PGobject();
			obj.setType("json");
			try {
				obj.setValue(student.getHostel_address().toString());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			proc.setObject(25, obj);
			proc.setString(26, student.getPermanent_address());
			proc.setString(27, student.getLocal_address());
			System.out.println(proc);
			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	public static long getRegistrationId(long erp_id) throws SQLException {
		PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"getRegistrationId\"(?);");
		proc.setLong(1, erp_id);
		ResultSet rs=proc.executeQuery();
	   rs.next();
	  long reg_id= rs.getLong(1);
		return reg_id;
	}
	
	public static void deleteRegisteredStudent(long csab_id) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"deleteRegisteredStudent\"(?);");
			proc.setLong(1, csab_id);

			proc.executeQuery();
			System.out.println("rows affected= " + proc.getUpdateCount());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static Integer getNewRegisteredStudentsCount() {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT \"getNewRegisteredStudentsCount\"();");
		
			ResultSet rs = proc.executeQuery();
			rs.next();
			Integer count = Integer.valueOf(rs.getInt(1));
			return count;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}
	public static Integer getNewReportedStudentsCount() {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT \"getNewReportedStudentsCount\"();");
		
			ResultSet rs = proc.executeQuery();
			rs.next();
			Integer count = Integer.valueOf(rs.getInt(1));
			return count;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

}
