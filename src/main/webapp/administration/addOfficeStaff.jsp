<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IIIT KOTA | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../plugins/select2/select2.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
 
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script>
    function autoSuggest(str){
    	
    		
    		var xmlhttp;
    		try{
    			xmlhttp = new XMLHttpRequest();
    		} catch (e){
    			// Internet Explorer Browsers
    			try{
    				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    			} catch (e) {
    				try{
    					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    				} catch (e){
    				//Browser doesn't support ajax	
    					alert("Your browser is unsupported");
    				}
    			}
    		}	
    		
    		if(xmlhttp){
    		    xmlhttp.onreadystatechange=function() {
    		    	
    		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
    					
    		        	a = JSON.parse(xmlhttp.responseText);
    		        	document.getElementsByClassName("form-control select2 select2-hidden-accessible")[0].innerHTML="";
    		        	
    		        	for(var i=0;i<a.length;i++){
    		        		document.getElementsByClassName("form-control select2 select2-hidden-accessible")[0].innerHTML+="<option>"+a[i].name+"</option>";
    		        		
    		        	}
    				}
    		        if(xmlhttp.status == 404)
    					alert("Could not connect to server");
    				}
    		    xmlhttp.open("POST","../AutoSuggest?string="+str,true);
    			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    		    xmlhttp.send();
    		}
    	   
    }	
    /*
    function getData(){
    	var users=[];
    	var json=[];
    	var a=[];
    	for(i=0;i<$("#user_list").select2('data').length;i++){
    	json[i]=JSON.stringify($("#user_list").select2('data')[i]);
    	a[i]=JSON.parse(json[i]);
    	}
    	for(var i=0;i<a.length;i++){
    		users[i]=a[i].id;
    		alert(users[i]);
    	}
    	var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
				//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		
		if(xmlhttp){
		    xmlhttp.onreadystatechange=function() {
		    	
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				}
		        if(xmlhttp.status == 404)
					alert("Could not connect to server");
				}
		    xmlhttp.open("POST","../CreateNewConversation?users="+users,true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    xmlhttp.send();
		}
	
    	
    	return false;
    }*/
  </script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<%@ include file="header.jsp" %>
 <!-- Left side column. contains the logo and sidebar -->
 <%@ include file="main-sidebar.jsp" %>
 
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Student
        <small>Home</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#" class="active"><i class="fa fa-dashboard"></i>Home</a></li>
      </ol>
    </section>

    <!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Create Office Account</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label>Department</label>
                  <select class="form-control" name="department">
                    <option value="">Select any One</option>
                    <option value="academics">Academics</option>
                    <option value="accounts">Accounts</option>
                    <option value="assistance">Assistance</option>
                  </select>
                  <div class="messages"></div>
                </div>
              
                <div class="form-group">
                  <label >First name</label>
                  <input type="text" class="form-control" placeholder="Enter First Name" name="first_name">
                  <div class="messages"></div>
                </div>
                <div class="form-group">
                  <label >Last name</label>
                  <input type="text" class="form-control" placeholder="Enter Last Name" name="last_name">
                  <div class="messages"></div>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Email</label>
                  <input type="email" class="form-control" name="email" >
                  <div class="messages"></div>
                </div>
                <div class="form-group">
                  <label >Username</label>
                  <input type="text" class="form-control" name="username" >
                  <div class="messages"></div>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" name="password" >
                  <div class="messages"></div>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Repeat Password</label>
                  <input type="password" class="form-control" name="repeat_password">
                  <div class="messages"></div>
                </div>
                
               
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="submit" onclick="getData()"   value="Create Account" class="btn btn-primary"/>
              
              </div>
            </form>
          </div>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <%@ include file="footer.jsp" %>
  <!-- Control Sidebar -->
  <%@ include file="control-sidebar.jsp" %>
 
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.5 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="../plugins/select2/suggestion.js"></script>

<script src="../plugins/select2/select2.full.min.js"></script>

<script src="../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);


</script>
</body>
</html>