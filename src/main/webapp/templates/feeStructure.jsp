<!DOCTYPE html>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="postgreSQLDatabase.feePayment.FeeBreakup"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<html>
<head>
<style>
@page {
	size: A4;
}
</style>
<style>
table, th, td {
	border: 1px solid black;
}
</style>
</head>

<body>
	
	<p align="center">
		<img alt=""
			src="../ftp/templates/IIITKLetterHead.png"
			style="height: 99px; width: 729px" />
	</p>
	<h2 align="center" style="text-decoration: underline">INSTITUTE
		FEE</h2>
	<p align="center">Fees Structure for B.Tech/B.Arch. students
		admitted in the session 2015-16</p>
	<br />
	<table width= "100%" style="border-collapse:collapse">
		<thead>
			<tr>
				<th rowspan="2">S.No.</th>
				<th rowspan="2">Head of Fees</th>
				<th colspan="2">Odd Semesters</th>
			</tr>
			<tr>
				<th>For General Category <br>Students(in Rupees)
				</th>
				<th>For SC/ST Category <br>Students(in Rupees)
				</th>
			</tr>
			<tr>
				<!-- <th colspan="2">A. Admission/Tuition Fees</th> -->
			</tr>
		</thead>
		<tbody>
			<%
			
			int sem=Integer.parseInt(request.getParameter("sem"));
			int year=Integer.parseInt(request.getParameter("year"));
			
				ArrayList<FeeBreakup> fee_breakup = postgreSQLDatabase.feePayment.Query.getFeeBreakup(sem,year);
				Iterator<FeeBreakup> iterator = fee_breakup.iterator();
				JSONArray general = null, sc = null;
				while (iterator.hasNext()) {

					FeeBreakup current = iterator.next();
					if (current.getCategory().equalsIgnoreCase("GENERAL"))
						general = current.getBreakup();
					if (current.getCategory().equalsIgnoreCase("SC"))
						sc = current.getBreakup();

				}

				//out.print(general.toString());
				int count = 1;
				for (int i = 0; i < general.length() - 1; i++) {
					JSONObject general_section = general.getJSONObject(i);
					JSONObject sc_section = general.getJSONObject(i);

					String header = general_section.keys().next();
					out.println("<tr><td colspan='4'><strong >" + header.toUpperCase() + "</strong></td></tr>");
					JSONArray general_fields = general_section.getJSONArray(header);
					JSONArray sc_fields = sc_section.getJSONArray(header);
					for (int j = 0; j < general_fields.length(); j++) {
						String field_header = general_fields.getJSONObject(j).keys().next();
						out.println("<tr><td>" + (count++) + "</td><td  style='font-weight:bold; !important'>" + field_header + "</td><td>"
								+ general_fields.getJSONObject(j).get(field_header) + "</td><td>"
								+ sc_fields.getJSONObject(j).get(field_header) + "</td></tr>");
					}

				}
				out.println(
						"<tr><td colspan='2'><strong>Total</strong></td><td><strong>" + general.getJSONObject(general.length() - 1).getLong("total")
								+ "</strong></td><td><strong>" + sc.getJSONObject(general.length() - 1).getLong("total") + "</strong></td></tr>");
			%>


		</tbody>
	</table>
</body>
</html>