<%@page import="users.Student"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<html>
<head>


<style>
table {
	border: 1px solid black;
	border-collapse: collapse;
	cell-spacing: 0px;
}

@page {
	size: A4;
}
</style>
</head>
<body>
	<title>REGISTRATION ID ALLOCATION</title>
	<p align="center">
		<img alt="" src="../image/IIITKLetterHead.png"
			style="height: 99px; width: 729px" />
	</p>

	<%
		Student student = null;
		if (request.getParameter("reg_id") != null)
			student = Query.getRegistrationStudentData(Long.parseLong(request.getParameter("reg_id")));
		else if (request.getParameter("csab_id") != null)
			student = Query.getCsabStudentProfile(Long.parseLong(request.getParameter("csab_id")));
		else
			response.sendError(500);
	%>

	<center>
		<h2>IIITK Registration Procedure</h2>
	</center>
	<p style="margin-left: 9%">
		Dear
		<%=student.getName()%></p>
	<p style="margin-left: 9%">Your Registration ID is</p>
	<center>
		<table>
			<tr>
				<td><span style="font-size: 150%; font-weight: bold;"><%=student.getRegistration_id()%></span></span></td>
			</tr>
		</table>
	</center>

	<h3 style="margin-left: 9%;">Guidelines for registration process.</h3>

	<ol type="1" style="margin-left: 9%;">
		<li><p>Make sure you have filed all Documents in the order of
				the checklist. You should have your payment receipt ready with you.</p></li>
		<li><p>Proceed to desk 1 and present your documents</p></li>
		<li><p>Login to ERP using above Registration ID.</p></li>
		<li><p>Update your information</p></li>
		<li><p>If you find any non-editable prefilled data incorrect
				contact the step 2 registration desk.</p></li>
		<li><p>Go to data verification desk,with all your documents
				and identity proofs for data verification</p></li>
		<li><p>Once your data is verified go to the</p></li>
		<li><p>Pay fees with your preffered medium.</p></li>
		<li><p>If you choose challan save the challan copy as PDF or
				request the desk for a printout.</p></li>
		<li><p>Logout and take the receipt to registration desk for
				verification</p></li>
		<li><p>On Confirmation go back to PC and choose your ERP
				username and password and have your student ID Registered.</p></li>
	</ol>
	<script>window.print()</script>
</body>
</html>




