<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="users.Student"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<html>
<head>
</head>

<style>
@page {
	size: A4;
}

th, td {
	border: 1px solid black;
	border-collapse: collapse;
	width: auto;
	margin-left: auto;
	margin-right: auto;
}

li {
	margin-left: 10%;
}

.tabl3 {
	border: 1px solid black;
	border-collapse: collapse;
	width: 100%;
}

}
h2 {
	display: block;
	font-size: 1.2em;
	margin-top: 0.67em;
	margin-bottom: 0.1em;
	margin-left: 0;
	margin-right: 0;
	font-weight: bold;
	text-decoration: underline;
}

th, td {
	padding: 5px;
	text-align: center;
}
</style>
</head>
<body>
	

	<%
		Student student = Query.getRegistrationStudentData(217l);
	%>
	<p><img alt="" src="../ftp/templates/IIITKLetterHead.png" style="height:99px; width:729px" /></p>

	<h2>
		<center>
			Documents
			<%=new SimpleDateFormat("yyyy").format(new Date()) %></center>
	</h2>
	</br>
	<center>
		<table style="width: 40%;">
			<tr style="border: none !important;">
				<td style="border: none !important; font-size: 120%;"><b>Student
						Name:</b></td>
				<td style="border: none !important; font-size: 120%;"><%=student.getName() %></td>
			</tr>
			<tr style="border: none !important;">
				<td style="border: none !important; font-size: 120%;"><b>Student
						Id:</b></td>
				<td style="border: none !important; font-size: 120%;"><%=student.getStudent_id() %></td>
			</tr>
			
		</table>
	</center>
	</br>




	<table class="tabl3" style="width: 100%";>
		<tr>
			<td>S.No.</td>
			<td>Particular</td>
			<td>Submitted Yes/No</td>
		</tr>
		<tr>
			<td>1.</td>
			<td>Provisional allotment letter issued by CSAB</td>
			<td></td>
		</tr>

		<tr>
			<td>2.</td>
			<td>JEE Main Score
				Card</td>
			<td></td>
		</tr>

		<tr>
			<td>3.</td>
			<td>Date of birth ceritficate(X Class pass ceritficate/marksheet</td>
			<td></td>
		</tr>

		<tr>
			<td>4.</td>
			<td>Mark sheets of qualifying examination</td>
			<td></td>
		</tr>

		<tr>
			<td>5.</td>
			<td>Character Certificate from the last school/Institute
				attended in original</td>
			<td></td>
		</tr>

		<tr>
			<td>6.</td>
			<td>Transfer certificate/migration ceritficate submitted in
				original</td>
			<td></td>
		</tr>

		<tr>
			<td>7.</td>
			<td>Medical Certificate issued by the recognized/reputed
				hospital</td>
			<td></td>
		</tr>

		<tr>
			<td>8.</td>
			<td> Income Certificate</td>
			<td></td>
		</tr>
		<tr>
			<td>9.</td>
			<td>Caste Certificate(SC/ST/OBC)</td>
			<td></td>
		</tr>

		<tr>
			<td>10.</td>
			<td>Certificate of physical handicapped (if applicable)</td>
			<td>....</td>
		</tr>

		<tr>
			<td>11.</td>
			<td>ID Proof(in case of Indian student Aadhar Card/Voter ID Card</td>
			<td></td>
		</tr>

	</table>
	</body>
	</html>