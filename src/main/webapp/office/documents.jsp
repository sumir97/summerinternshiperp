<!DOCTYPE html>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="postgreSQLDatabase.documents.Documents"%>
<%@page import="postgreSQLDatabase.documents.Query"%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIITK | ERP</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../plugins/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../plugins/ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">

<style>
.example-modal .modal {
	position: relative;
	top: auto;
	bottom: auto;
	right: auto;
	left: auto;
	display: block;
	z-index: 1;
}

.example-modal .modal {
	background: transparent !important;
}
</style>



<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="modal fade" id="profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	
</div>
	<div class="wrapper">

		<%@ include file="header.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>
		<%@ page import="java.util.ArrayList"%>
		<%@ page import="java.util.Iterator"%>
		<%@ page import="users.Student"%>



		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Student <small>Registration Status</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Tables</a></li>
					<li class="active">Data tables</li>
				</ol>
			</section>


			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Student List</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body" style="overflow-x: scroll;">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Delete</th>
											<th>Title</th>
											<th>Time</th>
											<th>Download File</th>
										</tr>
									</thead>
									<tbody>
										<%
											ArrayList<Documents> list = Query.getDocuments(Long.parseLong(request.getParameter("reg_id")));
											Iterator<Documents> iterator = list.iterator();
											SimpleDateFormat df = new SimpleDateFormat("hh:mm dd/MM/yyyy");
											while (iterator.hasNext()) {
												Documents current = iterator.next();
										%>
										<tr>
											<td>
												<button name="delete"
													onclick="deleteDocument(<%=current.getDocument_id()%>)"
													class="btn btn-warning">Delete</button>
											</td>
											<td><%=current.getDocument_name()%></td>

											<td><%=df.format(current.getTimestamp())%></td>
											<td><a
												href="../FtpDownload?file=<%=current.getFile().getDirectory() + "/" + current.getFile().getFile_name()%>"
												rel="nofollow"><button>Download</button></a></td>
										</tr>

										<%
											}
										%>
									</tbody>
								</table>
								<button class="btn btn-success" onclick="addIframe(<%=Long.parseLong(request.getParameter("reg_id"))%>)">Upload Profile Picture</button>
								<form
									action="../DocumentUpload?reg_id=<%=request.getParameter("reg_id")%>"
									method="POST" enctype="multipart/form-data">
									<div class="row col-md-12">
										<select class="form-control col-md-6" name="document_name"
											required>
											<option value="10th Mark Sheet">10th Mark Sheet</option>
											<option value="10th School Leaving certificate">10th
												School Leaving certificate</option>
											<option value="12th Mark Sheet">12th Mark Sheet</option>
											<option value="12th School Leaving certificate">12th
												School Leaving certificate</option>
											<option value="Passport">Passport</option>
											<option value="Ration Card">Ration Card</option>
											<option value="Aadhar Card">Aadhar Card</option>
											<option value="JEE Mains Admit Card">JEE Mains Admit
												Card</option>
											<option value="JEE Mains Marks sheet">JEE Mains
												Marks sheet</option>
											<option value="Health certificate">Health
												certificate</option>
												<option value="Gap Certificate">Gap
												Certificate</option>
												<option value="Income Certificate">Income
												Certificate</option>
											<option value="Transfer Certificate">Transfer
												Certificate</option>
											<option value="Character Certificate">Character
												Certificate</option>
											<option value="Caste certificate(SC/ST/OBC)">Caste
												certificate(SC/ST/OBC)</option>
												<option value="Other">Other</option>
											<option
												value="Certificate of physical Handicapped(if applicable)">Certificate
												of physical Handicapped(if applicable)</option>
										</select> <input type="file" name="button" value="Browse" required /><br />
										<input type="submit" name="Upload" value="Upload" class="btn btn-primary"/><br />
									</div>

								</form>


							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="footer.jsp"%>
		<!-- Control Sidebar -->
		<%@ include file="control-sidebar.jsp"%>
		<!-- /.control-sidebar -->

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../dist/js/demo.js"></script>
	<!-- page script -->
	<script src="../dist/js/payment.js"></script>
	<script>
  $(function () {
    $("#example1").DataTable({
		"paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
	});
  });
  
  
 
</script>
	<script>
 function deleteDocument(document_id){
	var xmlhttp;
	try{
		xmlhttp = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				//Browser doesn't support ajax	
				alert("Your browser is unsupported");
			}
		}
	}	
	//var xmlhttp=new XMLHttpRequest();

	if(xmlhttp){	
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				window.location.reload();
			}
			if(xmlhttp.status == 404)
				alert("Could not connect to server");
		}
		xmlhttp.open("POST","../DeleteDocument",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("document_id="+document_id);
	}
	return false;
  }
</script>
	<script>
function addIframe(reg_id){
	document.getElementById("profile").innerHTML = '<div style="margin-left:20%;margin-right:20%;margin-top:2%;"><iframe id="profile_pic" src="idCardImageUpload.jsp?reg_id='+reg_id+'" width="100%" height="600px" style="border:none;overflow:hidden;"></iframe></div>';
$("#profile").modal();
}
</script>
</body>
</html>